const express = require('express');
const redis = require('redis');

const app = express();
const port = 3000;

const client = redis.createClient({
  host: 'redis',
  port: 6379
});

app.use(express.json());

app.post('/message', (req, res) => {
  const message = req.body.message;
  if (!message) {
    return res.status(400).send('Message is required');
  }

  client.rpush('messages', message, (err, reply) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.send('Message added');
  });
});


app.get('/messages', (req, res) => {
  client.lrange('messages', 0, -1, (err, messages) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.json(messages);
  });
});
app.get('/', (req, res) => {
    client.lrange('messages', 0, -1, (err, messages) => {
      if (err) {
        return res.status(500).send(err);
      }
      res.json(messages);
    });
  });

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
